import * as dotenv from 'dotenv'
import '@nomiclabs/hardhat-ethers'
import { task, types } from 'hardhat/config'
import { HardhatRuntimeEnvironment as HRE } from 'hardhat/types'

import { ERC20Token } from '../typechain/ERC20Token'

dotenv.config()
const { DEPLOYED_CONTRACT_ADDRESS } = process.env

const TOKENS_TO_MINT = 100

async function getContractToken(hre: HRE) {
  const pumpkinTokenFactory = await hre.ethers.getContractFactory('ERC20Token')
  let pumpkinToken: ERC20Token

  if (DEPLOYED_CONTRACT_ADDRESS) {
    pumpkinToken = await pumpkinTokenFactory.attach(DEPLOYED_CONTRACT_ADDRESS)
  } else {
    throw Error('DEPLOYED_CONTRACT_ADDRESS not provided. Check .env!')
  }

  return pumpkinToken
}

task('balance', 'Get balance of account')
  .addParam('address', 'Account address', '', types.string)
  .setAction(async ({ address }, hre) => {
    const pumpkinToken = await getContractToken(hre)
    const balance = await pumpkinToken.balanceOf(address)

    console.log(`${address} balance is: ${balance}`)
  })

task('mint', 'Mint token to account')
  .addParam('address', 'The account address to mint to', '', types.string)
  .setAction(async ({ address }, hre) => {
    const pumpkinToken = await getContractToken(hre)

    await pumpkinToken.mint(address, TOKENS_TO_MINT)

    console.log(`Minted ${TOKENS_TO_MINT} to: ${address}`)
  })

task('approve', 'Add tokens allowance to account')
  .addParam('address', 'The spenders account address', '', types.string)
  .addParam('amount', 'Tokens amount to transfer', '', types.int)
  .setAction(async ({ address, amount }, hre) => {
    const pumpkinToken = await getContractToken(hre)
    await pumpkinToken.approve(address, amount)

    console.log(`Added ${amount} allowance to ${address}`)
  })

task('transfer', 'Transfer tokens to account')
  .addParam('address', 'The account address to transfer to', '', types.string)
  .addParam('amount', 'Tokens amount to transfer', '', types.int)
  .setAction(async ({ address, amount }, hre) => {
    const pumpkinToken = await getContractToken(hre)
    await pumpkinToken.transfer(address, amount)

    console.log(`Transfered ${amount} to: ${address}`)
  })

task('transferFrom', 'Transfer tokens from one account to another')
  .addParam('from', 'The account address to transfer from', '', types.string)
  .addParam('to', 'The account address to transfer to', '', types.string)
  .addParam('amount', 'Tokens amount to transfer', '', types.int)
  .setAction(async ({ from, to, amount }, hre) => {
    const pumpkinToken = await getContractToken(hre)
    await pumpkinToken.transferFrom(from, to, amount)

    console.log(`Transfered ${amount} from ${from} to ${to}`)
  })

export default {}
