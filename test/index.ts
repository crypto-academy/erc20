import { expect } from 'chai'
import { ethers } from 'hardhat'
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers'

import { ERC20Token } from '../typechain/ERC20Token'

const TOKEN_NAME = 'PumpkinToken'
const TOKEN_SYMBOL = 'PMT'
const TOKEN_DECIMALS = 8
const INITIAL_SUPPLY = 5000

// EVENT NAMES
const TRANSFER_EVENT = 'Transfer'
const APPROVE_EVENT = 'Approve'

// ERROR MESSAGES
const UNSUFFICENT_BALANCE = 'Not enough balance'
const TRANSFER_NOT_ALLOWED = 'Not allowed to transfer'
const INVALID_ADDRESS = 'Invalid address'
const OWNER_ONLY_ACCESS = 'Owner acceess required'

const ZERO_ADDRESS = '0x0000000000000000000000000000000000000000'

describe('ERC20Token', function () {
  let contract: ERC20Token
  let addr1ConnectedContract: ERC20Token
  let addr2ConnectedContract: ERC20Token

  let owner: SignerWithAddress
  let addr1: SignerWithAddress
  let addr2: SignerWithAddress

  beforeEach(async function () {
    const tokenFactory = await ethers.getContractFactory('ERC20Token')
    const addresses = await ethers.getSigners()

    owner = addresses[0]
    addr1 = addresses[1]
    addr2 = addresses[2]

    contract = await tokenFactory.deploy(INITIAL_SUPPLY)
    addr1ConnectedContract = await contract.connect(addr1)
    addr2ConnectedContract = await contract.connect(addr2)
  })

  /**
   * HELPER FUNCTIONS
   */

  // Modify next block timestamp

  /**
   * HELPER FUNCTIONS END
   */

  describe('name', function () {
    it('return correct token name', async () => {
      const name = await contract.name()

      expect(name).equal(TOKEN_NAME)
    })
  })

  describe('symbol', function () {
    it('return correct token symbol', async () => {
      const symbol = await contract.symbol()

      expect(symbol).equal(TOKEN_SYMBOL)
    })
  })

  describe('decimals', function () {
    it('return correct token decimals', async () => {
      const decimals = await contract.decimals()

      expect(decimals).equal(TOKEN_DECIMALS)
    })
  })

  describe('totalSupply', function () {
    it('returns totalySupply which equals to initialSupply passed to constructor', async () => {
      const totalSupply = await contract.totalSupply()

      expect(totalSupply).equal(INITIAL_SUPPLY)
    })
  })

  describe('balanceOf', function () {
    const ACCOUNT_TOKENS = 5000

    it('returns account balance', async () => {
      const tx = await contract.mint(addr1.address, ACCOUNT_TOKENS)
      await tx.wait()

      const balance = await contract.balanceOf(addr1.address)

      expect(balance).equal(ACCOUNT_TOKENS)
    })
  })

  describe('transfer', function () {
    const ACCOUNT_TOKENS = 5000
    const TRANSFER_TOKENS_AMOUNT = 2000
    const LEFT_TOKENS_AMOUNT = 3000

    async function makeTransfer() {
      const tx = await addr1ConnectedContract.transfer(
        addr2.address,
        TRANSFER_TOKENS_AMOUNT
      )
      await tx.wait()

      return tx
    }

    beforeEach(async function () {
      const tx = await contract.mint(addr1.address, ACCOUNT_TOKENS)
      await tx.wait()
    })

    it('transfer tokens from senders address to address2', async () => {
      await makeTransfer()

      const balanceAcc1 = await contract.balanceOf(addr1.address)
      const balanceAcc2 = await contract.balanceOf(addr2.address)

      expect(balanceAcc1).equal(LEFT_TOKENS_AMOUNT)
      expect(balanceAcc2).equal(TRANSFER_TOKENS_AMOUNT)
    })

    it('reverts when sender balance is unsufficient', async () => {
      const makeFailTransfer = async () =>
        await addr2ConnectedContract.transfer(
          addr1.address,
          TRANSFER_TOKENS_AMOUNT
        )

      await expect(makeFailTransfer()).to.be.revertedWith(UNSUFFICENT_BALANCE)
    })

    it('emits Transfer event wit correct arguments', async () => {
      const tx = await makeTransfer()

      expect(tx)
        .to.emit(contract, TRANSFER_EVENT)
        .withArgs(addr1.address, addr2.address, TRANSFER_TOKENS_AMOUNT)
    })
  })

  describe('transferFrom', function () {
    const ACCOUNT_TOKENS = 5000
    const TRANSFER_TOKENS_AMOUNT = 2000
    const LEFT_TOKENS_AMOUNT = 3000
    const ALLOWED_TRANSFER_AMOUNT = 4000

    async function makeTransferFrom() {
      const approveTx = await addr1ConnectedContract.approve(
        addr2.address,
        ALLOWED_TRANSFER_AMOUNT
      )
      await approveTx.wait()

      const tx = await addr1ConnectedContract.transferFrom(
        addr1.address,
        addr2.address,
        TRANSFER_TOKENS_AMOUNT
      )
      await tx.wait()

      return tx
    }

    beforeEach(async function () {
      const tx = await contract.mint(addr1.address, ACCOUNT_TOKENS)
      await tx.wait()
    })

    it('transfers tokens from address1 to address2', async () => {
      await makeTransferFrom()

      const balanceAcc1 = await contract.balanceOf(addr1.address)
      const balanceAcc2 = await contract.balanceOf(addr2.address)

      expect(balanceAcc1).equal(LEFT_TOKENS_AMOUNT)
      expect(balanceAcc2).equal(TRANSFER_TOKENS_AMOUNT)
    })

    it('transfers tokens and reduces allowance with send tokens amout', async () => {
      await makeTransferFrom()
      const allowance = await contract.allowance(addr1.address, addr2.address)

      expect(allowance).equal(ALLOWED_TRANSFER_AMOUNT - TRANSFER_TOKENS_AMOUNT)
    })

    it('reverts when sender balance is unsufficient', async () => {
      const approveTx = await addr2ConnectedContract.approve(
        addr1.address,
        TRANSFER_TOKENS_AMOUNT
      )
      await approveTx.wait()

      const makeFailTransfer = async () =>
        contract.transferFrom(
          addr2.address,
          addr1.address,
          TRANSFER_TOKENS_AMOUNT
        )

      await expect(makeFailTransfer()).to.be.revertedWith(UNSUFFICENT_BALANCE)
    })

    it('reverts when transfer is not allowed in allowances', async () => {
      const makeFailTransfer = async () =>
        contract.transferFrom(
          addr1.address,
          addr2.address,
          TRANSFER_TOKENS_AMOUNT
        )

      await expect(makeFailTransfer()).to.be.revertedWith(TRANSFER_NOT_ALLOWED)
    })

    it('emits Transfer event wit correct arguments', async () => {
      const tx = await makeTransferFrom()

      expect(tx)
        .to.emit(contract, TRANSFER_EVENT)
        .withArgs(addr1.address, addr2.address, TRANSFER_TOKENS_AMOUNT)
    })
  })

  describe('approve', function () {
    const APPROVED_TOKENS = 5000

    const makeApprovement = async () => {
      const tx = await addr1ConnectedContract.approve(
        addr2.address,
        APPROVED_TOKENS
      )
      await tx.wait()

      return tx
    }

    it('changes allowance of approved spender to given tokens amount', async () => {
      await makeApprovement()
      const allowance = await contract.allowance(addr1.address, addr2.address)
      expect(allowance).equal(APPROVED_TOKENS)
    })

    it('emits Approve event with correct arguments', async () => {
      const tx = makeApprovement
      expect(tx)
        .to.emit(contract, APPROVE_EVENT)
        .withArgs(addr1.address, addr2.address, APPROVED_TOKENS)
    })

    it('reverts for 0x0 address', async () => {
      const makeFailedApprove = async () =>
        addr1ConnectedContract.approve(ZERO_ADDRESS, APPROVED_TOKENS)

      await expect(makeFailedApprove()).to.be.revertedWith(INVALID_ADDRESS)
    })
  })

  describe('allowance', function () {
    const APPROVED_TOKENS = 5000

    beforeEach(async function () {
      const tx = await addr1ConnectedContract.approve(
        addr2.address,
        APPROVED_TOKENS
      )
      await tx.wait()
    })

    it('returns spender allowance for account owner', async () => {
      const allowance = await contract.allowance(addr1.address, addr2.address)
      expect(allowance).equal(APPROVED_TOKENS)
    })
  })

  describe('mint', function () {
    const MINT_TOKENS_AMOUNT = 3000

    const makeMint = async () => {
      const tx = await contract.mint(addr1.address, MINT_TOKENS_AMOUNT)
      await tx.wait()

      return tx
    }

    it('adds tokens to account', async () => {
      await makeMint()
      const balance = await contract.balanceOf(addr1.address)

      expect(balance).equal(MINT_TOKENS_AMOUNT)
    })

    it('adds tokens totalSupply', async () => {
      await makeMint()
      const totalSupply = await contract.totalSupply()

      expect(totalSupply).equal(INITIAL_SUPPLY + MINT_TOKENS_AMOUNT)
    })

    it('emits Transfer event wit correct arguments', async () => {
      const tx = await makeMint()

      expect(tx)
        .to.emit(contract, TRANSFER_EVENT)
        .withArgs(ZERO_ADDRESS, addr1.address, MINT_TOKENS_AMOUNT)
    })

    it('reverts for non-owner', async () => {
      const makeFailedMint = async () =>
        addr1ConnectedContract.mint(addr1.address, MINT_TOKENS_AMOUNT)

      await expect(makeFailedMint()).to.be.revertedWith(OWNER_ONLY_ACCESS)
    })

    it('reverts for 0x0 address', async () => {
      const makeFailedMint = async () =>
        contract.mint(ZERO_ADDRESS, MINT_TOKENS_AMOUNT)

      await expect(makeFailedMint()).to.be.revertedWith(INVALID_ADDRESS)
    })
  })

  describe('burn', function () {
    const MINT_TOKENS_AMOUNT = 3000
    const BURN_TOKENS_AMOUNT = 1500
    const TOTAL_SUPPLY = INITIAL_SUPPLY + MINT_TOKENS_AMOUNT

    const makeMint = async () => {
      const tx = await contract.mint(addr1.address, MINT_TOKENS_AMOUNT)
      await tx.wait()

      return tx
    }

    const makeBurn = async () => {
      const tx = await contract.burn(addr1.address, BURN_TOKENS_AMOUNT)
      await tx.wait()

      return tx
    }

    beforeEach(async function () {
      await makeMint()
    })

    it('removes burned tokens from account', async () => {
      await makeBurn()
      const balance = await contract.balanceOf(addr1.address)

      expect(balance).equal(MINT_TOKENS_AMOUNT - BURN_TOKENS_AMOUNT)
    })

    it('removes burned tokens from totalSupply', async () => {
      await makeBurn()
      const totalSupply = await contract.totalSupply()

      expect(totalSupply).equal(TOTAL_SUPPLY - BURN_TOKENS_AMOUNT)
    })

    it('emits Transfer event wit correct arguments', async () => {
      const tx = await makeMint()

      expect(tx)
        .to.emit(contract, TRANSFER_EVENT)
        .withArgs(addr1.address, ZERO_ADDRESS, BURN_TOKENS_AMOUNT)
    })

    it('reverts for non-owner', async () => {
      const makeFailedBurn = async () =>
        addr1ConnectedContract.burn(addr1.address, BURN_TOKENS_AMOUNT)

      await expect(makeFailedBurn()).to.be.revertedWith(OWNER_ONLY_ACCESS)
    })

    it('reverts if burning from unsuffient balance account', async () => {
      const makeFailedBurn = async () =>
        contract.burn(addr2.address, BURN_TOKENS_AMOUNT)

      await expect(makeFailedBurn()).to.be.revertedWith(UNSUFFICENT_BALANCE)
    })
  })
})
