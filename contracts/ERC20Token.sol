//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "hardhat/console.sol";


contract ERC20Token {
    address payable public immutable owner;
    string private _name = "PumpkinToken"; 
    string private _symbol = "PMT";
    uint private _decimals = 8;
    uint private _totalSupply;
    
    mapping(address => uint) private _balances;
    mapping(address => mapping(address => uint)) private _allowances;

    event Transfer(address indexed _from, address indexed _to, uint _value);
    event Approval(address indexed _owner, address indexed _spender, uint _value);
    
    
    constructor(uint initialSupply_) {
        owner = payable(msg.sender);
        _totalSupply = initialSupply_;
    }

    // Modifiers
    modifier onlyOwner() {
        require(msg.sender == owner, "Owner acceess required");
        _;
    }

    modifier isEnoughBalance(address account_, uint value_) {
        require(balanceOf(account_) >= value_, "Not enough balance");
        _;
    }

    modifier isCorrectAddress(address account_) {
        require(account_ != address(0), "Invalid address");
        _;
    }

    function name() public view returns(string memory) {
        return _name;
    }

    function symbol() public view returns(string memory) {
        return _symbol;
    }

    function decimals() public view returns(uint) {
        return _decimals;
    }

    function totalSupply() public view returns(uint) {
        return _totalSupply;
    }

    function balanceOf(address owner_) public view returns(uint) {
        return _balances[owner_];
    }

    function transfer(address to_, uint value_) public isEnoughBalance(msg.sender, value_) returns(bool) {
        _balances[msg.sender] -= value_;
        _balances[to_] += value_;
        
        emit Transfer(msg.sender, to_, value_);
        return true;
    }

    function transferFrom(address from_, address to_, uint value_) public isEnoughBalance(from_, value_) returns(bool) {
        // Check if required amount is allowed to transfer
        require(_allowances[from_][to_] >= value_, "Not allowed to transfer");

        _allowances[from_][to_] -= value_;
        _balances[from_] -= value_;
        _balances[to_] += value_;

        emit Transfer(from_, to_, value_);
        return true;
    }

    function approve(address spender_, uint value_) public isCorrectAddress(spender_) returns(bool) {
        _allowances[msg.sender][spender_] = value_;

        emit Approval(msg.sender, spender_, value_);

        return true;
    }
    
    function allowance(address owner_, address spender_) public view returns(uint) {
        return _allowances[owner_][spender_];
    }

    function mint(address to_, uint value_) public onlyOwner isCorrectAddress(to_) {
        _totalSupply += value_;
        _balances[to_] += value_;

        emit Transfer(address(0), to_, value_);
    }

    function burn(address from_, uint value_) public onlyOwner isCorrectAddress(from_) isEnoughBalance(from_, value_)  {
        _totalSupply -= value_;
        _balances[from_] -= value_;

        emit Transfer(from_, address(0), value_);
    }
}
